<?php

class m140627_201234_create_tag extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_tag', array(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'frequency' => 'integer DEFAULT 1',
        ));
	}

	public function down()
	{
		$this->dropTable('tbl_tag');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}