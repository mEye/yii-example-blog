<?php
class m140627_201509_insert_lookup_values extends CDbMigration
{
	public function up()
	{
        $this->insert('tbl_lookup', array(
            'name' => 'Draft',
            'type' => 'PostStatus',
            'code' => '1',
            'position' => '1',
        ));
        
        $this->insert('tbl_lookup', array(
            'name' => 'Published',
            'type' => 'PostStatus',
            'code' => '2',
            'position' => '2',
        ));
        
        $this->insert('tbl_lookup', array(
            'name' => 'Archived',
            'type' => 'PostStatus',
            'code' => '3',
            'position' => '3',
        ));
        
        $this->insert('tbl_lookup', array(
            'name' => 'Pending Approval',
            'type' => 'CommentStatus',
            'code' => '1',
            'position' => '1',
        ));
        
        $this->insert('tbl_lookup', array(
            'name' => 'Approved',
            'type' => 'CommentStatus',
            'code' => '2',
            'position' => '2',
        ));
	}

	public function down()
	{
		$this->delete('tbl_lookup', array(
            'name' => 'Draft',
            'type' => 'PostStatus',
            'code' => '1',
            'position' => '1',
        ));
        
        $this->delete('tbl_lookup', array(
            'name' => 'Published',
            'type' => 'PostStatus',
            'code' => '2',
            'position' => '2',
        ));
        
        $this->delete('tbl_lookup', array(
            'name' => 'Archived',
            'type' => 'PostStatus',
            'code' => '3',
            'position' => '3',
        ));
        
        $this->delete('tbl_lookup', array(
            'name' => 'Pending Approval',
            'type' => 'CommentStatus',
            'code' => '1',
            'position' => '1',
        ));
        
        $this->delete('tbl_lookup', array(
            'name' => 'Approved',
            'type' => 'CommentStatus',
            'code' => '2',
            'position' => '2',
        ));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}