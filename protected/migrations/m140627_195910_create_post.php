<?php

class m140627_195910_create_post extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_post', array(
            'id' => 'pk',
            'title' => 'string NOT NULL',
            'content' => 'text NOT NULL',
            'tags' => 'text',
            'status' => 'integer NOT NULL',
            'create_time' => 'integer',
            'update_time' => 'integer',
            'author_id' => 'integer NOT NULL',
        ));
        //Only for MySQL
        //$this->addForeignKey('FK_post_author', 'tbl_post', 'author_id', 'tbl_user', 'id', 'CASCADE', 'RESTRICT');
	}

	public function down()
	{
		$this->dropTable('tbl_post');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}