<?php

class m140627_200854_create_comment extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_comment', array(
            'id' => 'pk',
            'content' => 'text NOT NULL',
            'status' => 'integer NOT NULL',
            'create_time' => 'integer',
            'author' => 'string NOT NULL',
            'email' => 'string NOT NULL',
            'url' => 'string',
            'post_id' => 'integer NOT NULL',
        ));
        //Only for MySQL
        //$this->addForeignKey('FK_comment_post', 'tbl_comment', 'post_id', 'tbl_post', 'id', 'RESTRICT');
	}

	public function down()
	{
		$this->dropTable('tbl_comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}