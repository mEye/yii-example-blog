<?php

class m140627_210119_insert_user_data extends CDbMigration
{
	public function up()
	{
        $this->insert('tbl_user', array(
            'username' => 'demo',
            'password' => '$2a$10$JTJf6/XqC94rrOtzuF397OHa4mbmZrVTBOQCmYD9U.obZRUut4BoC',
            'email' => 'webmaster@example.com',
        ));
	}

	public function down()
	{
		$this->delete('tbl_user', array(
            'username' => 'demo',
            'password' => '$2a$10$JTJf6/XqC94rrOtzuF397OHa4mbmZrVTBOQCmYD9U.obZRUut4BoC',
            'email' => 'webmaster@example.com',
        ));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}