이 코드는 Yii Framework 파악을 위해서 [Building a Blog System using Yii](http://www.yiiframework.com/doc/blog/) 에 나오는 예제 코드대로 블로그를 작성한 것입니다.

# Step 0. 준비

## 블로그 기능 정의

### 소유자 기능

- 로그인, 로그아웃
- 포스트 작성, 갱신, 삭제
- 포스트 공개, 비공개, 아카이브
- 댓글 허용, 삭제

### 모든 사람(손님) 기능

- 포스트 읽기
- 댓글 작성

### 추가 기능

- 첫페이지는 가장 최근 포스트들이 리스팅되어야함.
- 10개를 초과한 포스트가 있다면 페이지 구분이 되어야함.
- 포스트는 댓글과 함께 표시되어야함.
- 특정 태그를 가진 포스트를 리스팅하는 기능.
- 태그의 사용 빈도에 따라서 클라우드로 표시.
- 최근 댓글의 목록을 표시
- 테마 기능
- SEO 친화적인 URL 사용

## Yii 1.1.14 설치

- yii 다운로드하여 압축해제
- framework 디렉토리를 프로젝트 디렉토리로 이동
- 스켈레톤 애플리케이션 생성

<pre><code>
protected/yiic webapp <blog> git
</code></pre>


# Step 1. 블로그 프로토타입 만들기

대부분의 기능을 구현

## Database 준비

```
protected/yiic migrate create create_tag
protected/yiic migrate
```

* http://www.yiiframework.com/doc/guide/1.1/en/database.migration
* http://www.yiiframework.com/doc/api/1.1/CDbSchema#getColumnType-detail
* http://www.yiiframework.com/doc/api/1.1/CDbMigration#addForeignKey-detail

## Gii 설치

* http://127.0.0.1:8080/index.php?r=gii

## 모델 생성

Gii 사용

1. Model Generator
2. tbl_ / user / Preview / Generate
3. tbl_user, tbl_post, tbl_comment, tbl_tag, tbl_lookup

## CRUD 만들기

Gii 사용

1. Crub Generator
2. Post / Preview / Generate
3. Post, Comment

* http://127.0.0.1:8080/index.php?r=post
* http://127.0.0.1:8080/index.php?r=comment

## 사용자 인증구현


# Step 2. 포스트 관리 기능

포스트 작성, 리스팅, 표시, 갱신, 삭제 구현

## 저장 create

Tag 관련 구현은 demo 보고 구현

## 표시 view

## 리스팅 index

## 관리 managing

## 삭제 delete


# Step 3. 댓글 관리 기능

댓글 작성, 리스팅, 허용, 갱신, 삭제 구현

## 표시 view

## 리스팅 index

## 관리 managing

## 삭제 delete


# Step 4. 포틀렛(portlets)

사용자 메뉴, 로그인, 태크클라우드, 최근 댓글 구현

## 사용자 메뉴

## 태그 클라우드

## 최근 댓글 목록


# Step 5. 마지막 조정과 배포

## URL 보기좋게 수정

## 기본 컨트롤러 설정

## 스키마 캐쉬 설정

## DEBUG 모드 삭제


# Final. 벤치마크 테스트

```
$ ab -c 2 -n 100 http://localhost/index.php
This is ApacheBench, Version 2.3 <$Revision: 1554214 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache/2.4.9
Server Hostname:        localhost
Server Port:            80

Document Path:          /index.php
Document Length:        4842 bytes

Concurrency Level:      2
Time taken for tests:   10.964 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      526400 bytes
HTML transferred:       484200 bytes
Requests per second:    9.12 [#/sec] (mean)
Time per request:       219.273 [ms] (mean)
Time per request:       109.636 [ms] (mean, across all concurrent requests)
Transfer rate:          46.89 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.5      0       5
Processing:   132  218  57.1    203     441
Waiting:      131  204  55.3    189     434
Total:        132  218  57.2    204     442

Percentage of the requests served within a certain time (ms)
  50%    204
  66%    227
  75%    248
  80%    257
  90%    286
  95%    343
  98%    439
  99%    442
 100%    442 (longest request)
```

# 기타 참고

## Vagrantfile

```
# -*- mode: ruby -*-
# vi: set ft=ruby :
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "precise32"
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.synced_folder "./", "/vagrant_data"
  config.vm.provision :shell, :path => "setup.sh"
end
```

## setup.sh

```
#!/bin/sh
perl -i -pe 's/us.archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list
DEBIAN_FRONTEND=noninteractive apt-get -q -y update
DEBIAN_FRONTEND=noninteractive apt-get -q -y install python-software-properties
DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:ondrej/php5
DEBIAN_FRONTEND=noninteractive aptitude -q -y update
DEBIAN_FRONTEND=noninteractive aptitude -q -y install libapache2-mod-php5 git php5-sqlite
```